/**
 * Translation table allows to specify lines to be translated on page.
 * Add you own line to the table in form of:
 *     '<line_id>': {
 *         'text': {
 *             '<your_locale>': '<your_translation>'
 *         }
 *     }
 * OR just add a new translation to already specified line:
 *     '#hello': {
 *         'es': 'Hola',
 *         '<your_locale>': '<your_translation>'
 *     }
 *
 */
var translationTable = {
    // Page title (Browser's tab)
    'title': {
        'text': {
            'en': 'Trade-Free',
            'de': 'Handelsfrei'
            'fr': 'Sans-Commerce'
        }
    },
    // Page title (logo text)
    '#title-trade-free': {
        'text': {
            'en': 'TRADE-FREE',
            'de': 'HANDELSFREI'
            'fr': 'SANS-COMMERCE'
        }
    },
    '#title-purest-form': {
        'text': {
            'en': 'the purest form of free',
            'de': 'Die reinste Form von frei'
        }
    },
    '#title-who-offer': {
        'text': {
            'en': 'the ones who offer, should not ask anything in return',
            'de': 'Diejenigen, die anbieten, sollten nichts im Gegenzug verlangen'
        }
    },
    '#title-who-receive': {
        'text': {
            'en': 'the ones who receive, should not have to give anything in return',
            'de': 'Diejenigen, die bekommen, sollten nichts zurückgeben müssen'
        }
    },
    '.player-container .text-downloading': {
        'text': {
            'en': 'Downloading',
            'de': 'Herunterladen'
        }
    },
    '.player-container .text-seeding': {
        'text': {
            'en': 'Seeding',
            'de': 'Seeden'
        }
    },
    '.player-container .text-from': {
        'text': {
            'en': 'from',
            'de': 'von'
        }
    },
    '.player-container .text-to': {
        'text': {
            'en': 'to',
            'de': 'an'
        }
    },
    '.player-container .text-peer': {
        'text': {
            'en': 'peer.',
            'de': 'Peer.'
        }
    },
    '.player-container .text-peers': {
        'text': {
            'en': 'peers.',
            'de': 'Peers.'
        }
    },
    '.player-container .text-of': {
        'text': {
            'en': 'of',
            'de': 'von'
        }
    },
    '.player-container .text-remaining': {
        'text': {
            'en': 'remaining.',
            'de': 'verbleiben.'
        }
    },
    '.player-container .text-done': {
        'text': {
            'en': 'Done.',
            'de': 'Erledigt.'
        }
    },
    '.player-container .text-download-externally': {
        'text': {
            'en': 'Download it externally.',
            'de': 'Extern herunterladen.'
        }
    },
    '#player1 .link-download-externally': {
        'link': {
            'en': 'magnet:?xt=urn:btih:bbe36cb6ba5d6afeb50421a2509a969786710e8e&dn=Trade-Free.mp4&tr=udp%3A%2F%2Fexplodie.org%3A6969&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Ftracker.empire-js.us%3A1337&tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969&tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337&tr=wss%3A%2F%2Ftracker.btorrent.xyz&tr=wss%3A%2F%2Ftracker.fastcast.nz&tr=wss%3A%2F%2Ftracker.openwebtorrent.com',
            'de': 'magnet:?xt=urn:btih:75c3f85c0c77d418f42600bbbf548cc1ebe2db7d&dn=Trade-Free+Video+german.mp4&tr=udp%3A%2F%2Fexplodie.org%3A6969&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Ftracker.empire-js.us%3A1337&tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969&tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337&tr=wss%3A%2F%2Ftracker.btorrent.xyz&tr=wss%3A%2F%2Ftracker.fastcast.nz&tr=wss%3A%2F%2Ftracker.openwebtorrent.com'
        }
    },
    '#player2 .link-download-externally': {
        'link': {
            'en': 'magnet:?xt=urn:btih:d66bf00649e889e261782c3a5098f68b68d35a00&dn=Trade-Free+Logo+Making.mp4&tr=udp%3A%2F%2Fexplodie.org%3A6969&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Ftracker.empire-js.us%3A1337&tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969&tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337&tr=wss%3A%2F%2Ftracker.btorrent.xyz&tr=wss%3A%2F%2Ftracker.fastcast.nz&tr=wss%3A%2F%2Ftracker.openwebtorrent.com',
            'de': 'magnet:?xt=urn:btih:b03d7dd9900edd4a4921044b4005807f110e17b8&dn=Trade-Free+Logo+Making+german.mp4&tr=udp%3A%2F%2Fexplodie.org%3A6969&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Ftracker.empire-js.us%3A1337&tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969&tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337&tr=wss%3A%2F%2Ftracker.btorrent.xyz&tr=wss%3A%2F%2Ftracker.fastcast.nz&tr=wss%3A%2F%2Ftracker.openwebtorrent.com'
        }
    },
    '#text-world-problems': {
        'text': {
            'en': 'We have many problems in the world today: corruption, climate change, violence, wars, monopolies, mafias, lack of healthcare, inequality, addiction and substance abuse, slavery, poorly made products and services, homelessness, environmental destruction, poor education, lack of funds for scientific developments, immigration, terrorism, famine, stress, crime and so on.',
            'de': 'Wir haben heute viele Probleme in der Welt: Korruption, Klimawandel, Gewalt, Kriege, Monopole, Mafias, Mangel an Gesundheitsversorgung, Ungleichheit, Sucht und Drogenmissbrauch, Sklaverei, schlechte Produkte und Dienstleistungen, Obdachlosigkeit, Umweltzerstörung, schlechte Bildung, fehlende Mittel für wissenschaftliche Entwicklung, Migration, Terrorismus, Hungersnot, Stress, Kriminalität und so weiter.'
        }
    },
    '#title-who-create-problems': {
        'text': {
            'en': 'Who/What creates these problems?',
            'de': 'Wer/Was erschafft diese Probleme?'
        }
    },
    '#text-humans': {
        'text': {
            'en': 'Humans.',
            'de': 'Menschen.'
        }
    },
    '#title-what-pushes': {
        'text': {
            'en': 'What pushes humans to behave like that?',
            'de': 'Was bringt Menschen dazu, sich so zu verhalten?'
        }
    },
    '#text-environment': {
        'text': {
            'en': 'Environment.',
            'de': 'Umfeld.'
        }
    },
    '#title-what-part': {
        'text': {
            'en': 'What part of the environment?',
            'de': 'Welcher Teil des Umfelds?'
        }
    },
    '#text-trade': {
        'text': {
            'en': 'Trade.',
            'de': 'Handel.'
        }
    },
    '#text-trade-is-problem': {
        'text': {
            'en': 'In short, trade creates most of the problems we see in the world today, and we want to make it obsolete by creating trade-free goods and services.',
            'de': 'Kurz gesagt, Handel erschafft die meisten Probleme, die wir heute in der Welt sehen, und wir wollen ihn durch das Erschaffen von handelsfreien Waren und Dienstleistungen obsolet machen.'
        }
    },
    '#text-toxic-environment': {
        'text': {
            'en': 'This will remove the toxic environment that pushes people to create problems.',
            'de': 'Dadurch wird das schädliche Umfeld beseitigt, das Menschen dazu bringt, Probleme zu erzeugen.'
        }
    },
    '#what-is-trade span.vc_tta-title-text': {
        'text': {
            'en': 'what is trade?',
            'de': 'Was ist Handel?'
        }
    },
    '#text-goods-and-services': {
        'text': {
            'en': 'If you create a good/service and you only allow people to use it if they give something in return, then that is a trade-based good/service.',
            'de': 'Wenn du eine Ware/Dienstleistung erschaffst und Menschen nur erlaubst, sie zu nutzen, wenn sie etwas zurückgeben, dann ist das eine handelsbasierte Ware/Dienstleistung.'
        }
    },
    '#text-society-is-trade-based': {
        'text': {
            'en': 'Pretty much our entire world-wide society is based on trades. Communism, socialism, capitalism, fascism, or any other political/governing systems are/were implemented as a layer on top of this trade-based environment. You, your parents or kids, friends and everyone else has to trade time, energy, skills, stuff, data, attention and so forth in order to get access to what they need and want: healthcare, food, shelter, comfort, gadgets, etc.. Currencies like money, bitcoin or any cryptocurrencies, social credits and the like, are all representations of this simple process of trade. Jobs and citizenship are the most well known means to officially trade in this society.',
            'de': 'So gut wie unsere gesamte weltweite Gesellschaft basiert auf dem Handel. Kommunismus, Sozialismus, Kapitalismus, Faschismus oder andere politische/verwaltende Systeme werden/wurden als eine Schicht auf dieses handelsbasierte Umfeld implementiert. Du, deine Eltern oder Kinder, Freunde und alle anderen müssen Zeit, Energie, Fähigkeiten, Dinge, Daten, Aufmerksamkeit und so weiter tauschen/handeln, um Zugang zu dem zu bekommen, was ihr/sie braucht/brauchen und wollt/wollen: Gesundheitsversorgung, Nahrung, Unterkunft, Komfort, Gadgets etc. Währungen wie Geld, Bitcoin oder irgendwelche Kryptowährungen, Sozialkredite und dergleichen sind allesamt Repräsentationen dieses einfachen Handelsprozesses. Arbeitsplätze und Staatsbürgerschaft sind das bekannteste Mittel, um offiziell in dieser Gesellschaft zu handeln.'
        }
    },
    '#text-facebook': {
        'text': {
            'en': 'In essence if you create a social network but you need people’s attention and/or data in order for users to use it, then that is a trade-based social network. An example of that is Facebook.',
            'de': 'Im Wesentlichen, wenn du ein soziales Netzwerk erstellst, du aber die Aufmerksamkeit und/oder Daten von Menschen benötigst, damit die Benutzer es nutzen können, dann ist das ein handelsbasiertes soziales Netzwerk. Ein Beispiel dafür ist Facebook.'
        }
    },
    '#link-facebook-wiki': {
        'link': {
            'en': 'https://en.wikipedia.org/wiki/Facebook',
            'de': 'https://de.wikipedia.org/wiki/Facebook'
        }
    },
    '#text-mastodon': {
        'text': {
            'en': 'A counter example, that of a trade-free social network, is Mastodon.',
            'de': 'Ein Gegenbeispiel, das eines handelsfreien sozialen Netzwerks, ist Mastodon.'
        }
    },
    '#link-mastodon-wiki': {
        'link': {
            'en': 'https://en.wikipedia.org/wiki/Mastodon_(software)',
            'de': 'https://de.wikipedia.org/wiki/Mastodon_%28Software%29'
        }
    },
    '#text-doctors-without-borders': {
        'text': {
            'en': 'A healthcare system that requires humans to give currency or their freedom (citizenship) in return for the service, is also a trade-based healthcare system. A counter example may be Doctors Without Borders who provide a trade-free healthcare service for those in need.',
            'de': 'Ein Gesundheitssystem, das von Menschen verlangt, dass sie im Gegenzug für die Dienstleistung Geld oder ihre Freiheit (Staatsbürgerschaft) geben, ist ebenfalls ein handelsbasiertes Gesundheitssystem. Ein Gegenbeispiel könnten Ärzte ohne Grenzen sein, die einen handelsfreien Gesundheitsdienst für Menschen in Not bereitstellen.'
        }
    },
    '#link-doctors-without-borders-wiki': {
        'link': {
            'en': 'https://en.wikipedia.org/wiki/Doctors_Without_Borders',
            'de': 'https://de.wikipedia.org/wiki/%C3%84rzte_ohne_Grenzen'
        }
    },
    '#why-is-trade-bad span.vc_tta-title-text': {
        'text': {
            'en': 'why is trade bad?',
            'de': 'Warum ist Handel schlecht?'
        }
    },
    '#text-imbalance-of-power': {
        'text': {
            'en': 'While trade was a necessary tool for our societies to evolve, it also creates an imbalance of power between people.',
            'de': 'Während Handel zwar ein notwendiges Mittel für die Entwicklung von Gesellschaften war, schafft er auch ein Ungleichgewicht von Macht zwischen Menschen.'
        }
    },
    '#text-facebook-advertising': {
        'text': {
            'en': 'Facebook is very inclined to collect more and more data from people, and trap as much of their attention as possible, since Facebook makes around 90% of their profits from advertising.',
            'de': 'Facebook ist sehr geneigt, mehr und mehr Daten von Menschen zu sammeln und so viel wie möglich von ihrer Aufmerksamkeit einzufangen, da Facebook ~90% seiner Gewinne aus Werbung macht.'
        }
    },
    '#text-users-trade-their-data': {
        'text': {
            'en': '“Data + Attention” = “More and Better Advertising” = “More Currency and Opportunities for Facebook“. Users trade their data and attention to Facebook in return for their social network’s features, and Facebook collects and trades all of that for currency. Thus the reason we see Facebook being more inclined to put their profits first (their trade advantages) and their users second. Same happens to Google and pretty much any other platform or service that relies on trades: from healthcare to food production and distribution, education, and so forth.',
            'de': '“Daten + Aufmerksamkeit” = “Mehr und bessere Werbung” = “Mehr Währung und Möglichkeiten für Facebook“. Die Nutzer handeln ihre Daten und Aufmerksamkeit auf Facebook als Gegenleistung für die Funktionen ihres sozialen Netzwerks, und Facebook sammelt und handelt all das gegen Geld. Aus diesem Grund sehen wir Facebook eher geneigt, ihre Gewinne an die erste Stelle zu setzen (ihre Handelsvorteile) und ihre Nutzer an die zweite Stelle. Das Gleiche passiert mit Google und so ziemlich jeder anderen Plattform oder Dienstleistung, die auf den Handel angewiesen ist: vom Gesundheitswesen über die Lebensmittelproduktion und -verteilung bis hin zur Bildung und so weiter.'
        }
    },
    '#text-abundance-of-stuff': {
        'text': {
            'en': 'This imbalance of power pushes people to lie, exaggerate claims, bribe others, create poor goods and services, push consumerism to new heights, and so forth. On top of this, considering that we already have an abundance of goods and services in the world, trade is an obsolete means to distribute this abundance.',
            'de': 'Dieses Ungleichgewicht von Macht bringt Menschen dazu, zu lügen, Behauptungen zu übertreiben, andere zu bestechen, schlechte Waren und Dienstleistungen zu schaffen, Konsum auf neue Höhen zu treiben und so weiter. Darüber hinaus ist der Handel, wenn man bedenkt, dass wir auf der Welt bereits einen Überfluss von Waren und Dienstleistungen haben, ein obsoletes Mittel, um diesen Überfluss zu verteilen.'
        }
    },
    '#link-trom-site-books': {
        'link': {
            'en': 'https://www.tromsite.com/books/',
            'de': 'https://www.tromsite.com/de/#books'
        }
    },
    '#why-trade-free span.vc_tta-title-text': {
        'text': {
            'en': 'why trade-free?',
            'de': 'Warum handelsfrei?'
        }
    },
    '#text-greatest-form-of-charity': {
        'text': {
            'en': 'Because it is the greatest form of charity and will lead to an abundance of goods and services in any domain of the society, if practiced by many and for long enough.',
            'de': 'Weil es die größte Wohltätigkeitsform ist und zu einem Überfluss von Waren und Dienstleistungen in jedem Bereich der Gesellschaft führen wird, wenn sie von vielen und lange genug praktiziert wird.'
        }
    },
    '#text-help-others-and-yourself': {
        'text': {
            'en': 'You help people but ask for nothing in return. You create software and share it with the world without asking for their data, attention, or currencies. You develop a healthcare system that caters for humans without asking for anything in return. You create and offer, and thus you help others and yourself. Others because they will get access to trade-free goods and services, and yourself because there will be no force dragging you into “unethical” and profit-oriented behaviors. By creating trade-free goods or services you are the utmost charitable creature there is.',
            'de': 'Du hilfst Menschen, verlangst aber nichts im Gegenzug. Du erstellst Software und teilst sie mit der Welt, ohne nach deren Daten, Aufmerksamkeit oder Währungen zu fragen. Du entwickelst ein Gesundheitssystem, das sich um Menschen kümmert, ohne etwas dafür zu verlangen. Du erschaffst und bietest an, und so hilfst du anderen und dir selbst. Anderen, weil sie Zugang zu handelsfreien Waren und Dienstleistungen erhalten, und dir selbst, weil es keine Kraft gibt, die dich in "unethisches" und gewinnorientiertes Verhalten zieht. Durch das Erschaffen von handelsfreien Waren oder Dienstleistungen bist du das größte wohltätige Lebewesen, das es gibt.'
        }
    },
    '#text-trade-free-society': {
        'text': {
            'en': 'A society where most of what people need and want is offered as trade-free, is a society void of most problems we see in the world today because there will be little to no incentive for people to create these problems in the first place.',
            'de': 'Eine Gesellschaft, in der das meiste von dem, was Menschen brauchen und wollen, handelsfrei angeboten wird, ist eine Gesellschaft, in der die meisten Probleme, die wir heute in der Welt sehen, nicht vorhanden sind, weil es wenig bis gar keinen Anreiz für Menschen geben wird, diese Probleme überhaupt erst zu schaffen.'
        }
    },
    '#text-trom-book': {
        'text': {
            'en': 'For a detailed explanation of what trade is, how it creates most of the world’s problems, and how to go about tackling it, we recommend the trade-free book “The Origin of Most Problems” by TROM.',
            'de': 'Für eine ausführliche Erklärung, was Handel ist, wie er die meisten Probleme der Welt verursacht und wie man ihn angeht, empfehlen wir das handelsfreie Buch “Die Ursache der meisten Probleme” von TROM.'
        }
    },
    '#link-trom-book': {
        'link': {
            'en': 'https://www.tromsite.com/books/#dflip-df_6562/1',
            'de': 'https://www.tromsite.de/buecher/#dflip-df_267/1'
        }
    },
    '#text-trade-free-logo-video': {
        'text': {
            'en': 'If you are creating trade-free goods and services you can label them as such (use the hand-print logo if you wish) and link to this website, so that people better understand the concept. If you want to see the story-idea about the logo, see this short video.',
            'de': 'Wenn du handelsfreie Waren und Dienstleistungen erschaffst, kannst du sie als solche kennzeichnen (verwende das Handabdruck-Logo, wenn du willst) und auf diese Website verlinken, damit andere das Konzept besser verstehen. Wenn du die Geschichte/Idee über das Logo sehen möchtest, schau dir dieses kurze Video an.'
        }
    },
    '#text-download-entire-page': {
        'text': {
            'en': 'Feel free to download this entire page and all it contains and post it anywhere you want.',
            'de': 'Du kannst die gesamte Seite mit allem, was sie enthält, herunterladen und veröffentlichen, wo du willst.'
        }
    },
    '#link-download-site-github': {
        'link': {
            'en': 'https://gitlab.com/tromsite/trade-free/trade-free.org',
            'de': 'https://gitlab.com/tromsite/trade-free/handelsfrei.org'
        }
    },
    '#text-dat-network': {
        'text': {
            'en': 'This website is also available on the DAT Network (100% decentralized).',
            'de': 'Diese Website ist auch im DAT Netzwerk verfügbar (100% dezentral).'
        }
    },
    '#link-dat-site': {
        'link': {
            'en': 'dat://c9d73e48758b06148c276651fe9755227c31ce113689cf58a0da6fe97c628931/',
            'de': 'dat://5c466431a91c6b4fb75a0114b61420622f645e7f672ec16b6f4b22d7acc593fd/'
        }
    }
};
